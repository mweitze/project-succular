# succular

Mobile Banking App Skeleton

## Getting Started

* Install Android SDK (Android Studio)
* Install git scm
* Install VS Code (or use Android Studio or any other IDE compatible with dart and flutter)
* Download and install Flutter
* Add Flutter sdk to path
* Create Emulator (Android Studio AVD Manager) or attach your own device with developer tools activated

## Authors
  
This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.





TEST