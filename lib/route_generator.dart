import 'dart:developer';

/**
 * This router should be used as the app is getting more complex
 */

/*  
import 'package:flutter/material.dart';
import 'package:succular/screens/home/home.dart';
import 'package:succular/screens/timeline/timeline.dart';
import 'package:succular/screens/analyze/analyze.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case '/':
         return MaterialPageRoute(
              builder: (_) => HomeScreen());
      // HOME
      case '/analyze':
        if (args is int) {
          return MaterialPageRoute(
              builder: (_) => AnalyzeScreen(navBarIndex: args));
        }
        return _errorRoute();
      // ANALYZE
      case '/timeline':
        if (args is int) {
          return MaterialPageRoute(
              builder: (_) => TimelineScreen(navBarIndex: args));
        }
        return _errorRoute();

      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
          appBar: AppBar(title: Text("Error")),
          body: Center(
            child: Text("ERROR"),
          ));
    });
  }
}
*/