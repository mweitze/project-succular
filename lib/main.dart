import 'dart:developer';

import 'package:flutter/material.dart';
// import 'package:succular/route_generator.dart';
import 'package:succular/Styles/fonts.dart';
import 'package:succular/screens/pageLoader/home.dart';


void main() => runApp(BankingApp());

class BankingApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: HomeScreen(),
        theme: ThemeData(
            appBarTheme:
                AppBarTheme(textTheme: TextTheme(title: AppBarTextStyle)),
            textTheme: TextTheme(
              //tiel and body = color black
              title: TitleTextStyle,
              body1: Body1TextStyle,
              // deadlines and display = color white
              headline: Header1,
              display1: display1,
            )));
  }
}
