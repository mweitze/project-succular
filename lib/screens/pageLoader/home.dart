import 'package:circular_bottom_navigation/tab_item.dart';
import 'package:flutter/material.dart';
import 'package:circular_bottom_navigation/circular_bottom_navigation.dart';
import 'package:succular/screens/analyze/analyze.dart';
import 'package:succular/screens/overview/overview.dart';
import 'package:succular/screens/overview/timeline/timeline.dart';
import 'package:succular/screens/transfer/main_transfer.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int selectedPos = 0;
  
  @override
  BuildContext get context => super.context;

  final _pages = [
    MainTransferScreen(),
    OverviewScreen(),
    AnalyzeScreen(),
    TimelineScreen(),
  ];

  double bottomNavBarHeight = 50;

  List<TabItem> tabItems = List.of([
    new TabItem(Icons.swap_vert, "Transfer", Color.fromRGBO(0, 89, 162, 100),labelStyle: TextStyle(fontWeight: FontWeight.normal)),
    new TabItem(Icons.account_balance_wallet, "Overview", Color.fromRGBO(0, 89, 162, 100),labelStyle: TextStyle(fontWeight: FontWeight.normal)),
    new TabItem(Icons.equalizer, "Analyze", Color.fromRGBO(0, 89, 162, 100),labelStyle: TextStyle(fontWeight: FontWeight.normal)),
    new TabItem(Icons.layers, "Goals", Color.fromRGBO(0, 89, 162, 100),labelStyle: TextStyle(fontWeight: FontWeight.normal)),
    new TabItem(Icons.more_horiz, "More", Color.fromRGBO(0, 89, 162, 100),labelStyle: TextStyle(fontWeight: FontWeight.normal)),
  ]);

  CircularBottomNavigationController _navigationController;

  @override
  void initState() {
    super.initState();
    _navigationController = new CircularBottomNavigationController(selectedPos);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Padding(child: bodyContainer(), padding: EdgeInsets.only(bottom: bottomNavBarHeight),),
          Align(alignment: Alignment.bottomCenter, child: bottomNav())
        ],
      ),
    );
  }

  Widget bodyContainer() {
    return GestureDetector(
      child: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.white,
        child: Center(
          child: _pages[selectedPos],
          ),
        ),
      onTap: () {
        if (_navigationController.value == tabItems.length - 1) {
          _navigationController.value = 0;
        } else {
          _navigationController.value++;
        }
      },
    );
  }

  Widget bottomNav() {
    return CircularBottomNavigation(
      tabItems,
      controller: _navigationController,
      barHeight: bottomNavBarHeight,
      barBackgroundColor: Colors.white,
      animationDuration: Duration(milliseconds: 250),
      circleSize: 42,
      iconsSize: 30,
      circleStrokeWidth: 0,
      selectedCallback: (int selectedPos) {
        setState(() {
          this.selectedPos = selectedPos;
          print(_navigationController.value);
        });
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    _navigationController.dispose();
  }
}