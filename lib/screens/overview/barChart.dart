import "package:flutter/material.dart";
import 'package:percent_indicator/linear_percent_indicator.dart';

class StackedIncomeOutcomeBarChart extends StatelessWidget {
  // PRIVATE MEMBERS START WITH UNDERSCORE
  final String _currency;
  final double _income; 
  final double _outgoing;
  String _infotext;
  double _percent;

  StackedIncomeOutcomeBarChart(this._income, this._outgoing, this._currency){
    _percent = _outgoing/ _income;
    if (_outgoing > _income) {
      _percent = 1.0;
       _infotext = "Cash Out (" + _outgoing.toString() + _currency + ") > Cash In (" + _income.toString() + _currency + ")";
    } else {
      _percent = _outgoing / _income;
      _infotext = "Cash Out (" + _outgoing.toString() + _currency + ") < Cash In (" + _income.toString() + _currency + ")";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.all(0),
              child: new LinearPercentIndicator(
                backgroundColor: Colors.greenAccent,
                animation: true,
                animationDuration: 1000,
                lineHeight: 20.0,
                percent: _percent,
                center: Text(_infotext, style: TextStyle(fontSize:10)),
                linearStrokeCap: LinearStrokeCap.roundAll,
                progressColor: Colors.redAccent,
              ),
      ),
    );
  }
}
