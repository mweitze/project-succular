import 'package:flutter/foundation.dart';
import "package:flutter/material.dart";

import 'barChart.dart';

class BankSection extends StatelessWidget {
  // PRIVATE MEMBERS START WITH UNDERSCORE

  final String _accountName;
  final String _currency;
  final Color _color;
  int _id;

  
  final double _balance;
  final double _income; 
  final double _outgoing;

  BankSection(this._accountName, this._color, this._currency, this._balance, this._income, this._outgoing, this._id, {Key key});

  @override
  Widget build(BuildContext context) {

    return Container(
      key: key,
      decoration: BoxDecoration(
        //color:_color,
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment(0.5, 0.0),// 10% of the width, so there are ten blinds.
          colors: [const Color.fromRGBO(14, 74, 164, 100), const Color.fromRGBO(0, 0, 200, 100)], // whitish to gray
          //colors: [const Color.fromRGBO(0, 0, 0, 100), const Color.fromRGBO(8, 61, 219, 100)], 
          tileMode: TileMode.clamp, // repeats the gradient over the canvas
        ),
        borderRadius: BorderRadius.circular(25),
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 9.0,
            offset: Offset(0, 3),
          ),
        ],
      ),
      margin: EdgeInsets.symmetric(
        horizontal: 15.0, vertical: 7.0),
      child:
      Column(
        children: [
          Row(
            children: [
               Flexible(
                fit: FlexFit.loose,
                flex: 1,
                child: Container(
                  alignment: Alignment.topLeft,
                  padding: EdgeInsets.all(5),
                  child: Icon(
                    Icons.blur_on,
                    size: 50.0,
                    color: Colors.white,
                  ),
                ),
               ),
             Flexible(
                fit: FlexFit.tight,
                flex: 5,
                child: Container(
                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                alignment: Alignment.centerLeft,
                child:Text("wallets", style: TextStyle(color:Colors.white, fontSize: 15, fontStyle: FontStyle.italic),
                ),
              ),
             ),
              Container(
                alignment: Alignment.topRight,
                padding:  EdgeInsets.fromLTRB(10, 0, 20, 0),
                child: Icon(
                  Icons.more_horiz,
                  size: 22.0,
                  color: Colors.white70,
                )
              ),
            ]
          ),
          Container(
                padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                child:Text(_accountName, style: Theme.of(context).textTheme.headline),
              ),
           Container(
            alignment: Alignment.bottomRight,
            padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
            child:Text("BALANCE", style: TextStyle(fontSize: 10, color: Colors.white70)),
          ),
          Container(
            alignment: Alignment.bottomRight,
            padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
            child:Text(_balance.toStringAsFixed(2) + " " + _currency, style: Theme.of(context).textTheme.display1),
          ),
          Container(
            alignment: Alignment.bottomLeft,
            padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
            child: Text("CASH FLOW", style: TextStyle(fontSize: 10, color: Colors.white70))
          ),
          Container(
            alignment: Alignment.bottomCenter,
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: StackedIncomeOutcomeBarChart(5.0,3.0, _currency),
          ),
        ],
      )
  );
  }
}
