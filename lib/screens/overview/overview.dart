import "package:flutter/material.dart";
import 'package:succular/models/bankaccount.dart';
import 'package:succular/screens/overview/bank_section.dart';
import "../../models/accountGroup.dart";

class OverviewScreen extends StatefulWidget {
  @override
  _OverviewScreenState createState() => _OverviewScreenState();
}
class _OverviewScreenState extends State<OverviewScreen> {

  List<BankAccount> accounts = AccountGroup.bankSections();

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 30, 10, 30),
      child: ReorderableListView(
        children: List.generate(accounts.length, (index) {
            return Container(
              height: 185.0,
              key: ValueKey(accounts[index].id.toString()),
              child: BankSection(
                accounts[index].name,
                accounts[index].color,
                accounts[index].currency, 
                accounts[index].balance,
                accounts[index].income,
                accounts[index].outgoing,
                accounts[index].id,
                key: Key(accounts[index].id.toString())
            ),
            );
            
        }),
        onReorder: (int start, int current) {
          setState(() {
            _updateListItems(start, current);
          });
        },
      ),
    );
  }

  void _updateListItems(int oldIndex, int newIndex) {
    if(newIndex > oldIndex){
      newIndex -= 1;
    }

    final BankAccount item = accounts.removeAt(oldIndex);
    accounts.insert(newIndex, item);

  }
}
