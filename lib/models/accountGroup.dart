import "package:flutter/material.dart";
import "bankaccount.dart";

class AccountGroup {
  String name;
  final List<BankAccount> bankAccounts;

  AccountGroup(this.name, this.bankAccounts);

  static List<BankAccount> bankSections() {
    return [
      BankAccount(
          "Postbank", Colors.blue[300], "EUR", 2450.00, 2000.50, 449.50, 1),
      BankAccount(
          "Deutsche Bank", Colors.red[300], "EUR", -150.00, 2000, 2150, 2),
      BankAccount(
          "ING Diba", Colors.green[300], "EUR", 10000.00, -200.00, 10200.00, 3),
      BankAccount(
          "Sparkasse", Colors.yellow[300], "EUR", 50000.00, 55000.00, -5000, 4),
      BankAccount("Bamberger Bank", Colors.green[900], "EUR", 500000.00,
          55000.00, 100000, 5),
      BankAccount(
          "Postbank", Colors.blue[300], "EUR", 2450.00, 2000.50, 449.50, 6),
      BankAccount(
          "Deutsche Bank", Colors.red[300], "EUR", -150.00, 2000, 2150, 7),
      BankAccount(
          "ING Diba", Colors.green[300], "EUR", 10000.00, -200.00, 10200.00, 8),
      BankAccount(
          "Sparkasse", Colors.yellow[300], "EUR", 50000.00, 55000.00, -5000, 9),
      BankAccount("Bamberger Bank", Colors.green[900], "EUR", 500000.00,
          55000.00, 100000, 10)
    ];
  }

  static List<AccountGroup> fetchAll() {
    return [
      AccountGroup(
        "All",
        [
          BankAccount(
              "Postbank", Colors.blue[300], "EUR", 2450.00, 2000.50, 449.50, 1),
          BankAccount(
              "Deutsche Bank", Colors.red[300], "EUR", -150.00, 2000, 2150, 2),
          BankAccount("ING Diba", Colors.green[300], "EUR", 10000.00, -200.00,
              10200.00, 3),
          BankAccount("Sparkasse", Colors.yellow[300], "EUR", 50000.00,
              55000.00, -5000, 4),
          BankAccount("Bamberger Bank", Colors.green[900], "EUR", 500000.00,
              55000.00, 100000, 5),
          BankAccount(
              "Postbank", Colors.blue[300], "EUR", 2450.00, 2000.50, 449.50, 6),
          BankAccount(
              "Deutsche Bank", Colors.red[300], "EUR", -150.00, 2000, 2150, 7),
          BankAccount("ING Diba", Colors.green[300], "EUR", 10000.00, -200.00,
              10200.00, 8),
          BankAccount("Sparkasse", Colors.yellow[300], "EUR", 50000.00,
              55000.00, -5000, 9),
          BankAccount("Bamberger Bank", Colors.green[900], "EUR", 500000.00,
              55000.00, 100000, 10)
        ],
      )
    ];
  }
}
