import "package:flutter/material.dart";


class BankAccount {
  final String name; 
  String currency;
  final Color color;
  
  final double balance;
  final double income;
  final double outgoing;
  int id;

  BankAccount(this.name, this.color, String currency, this.balance, this.income, this.outgoing, this.id) {
    switch(currency) {
      case "EUR": this.currency = "€"; break;
      case "USD": this.currency = "USD"; break;
      default: this.currency = "€";
    }
  }
}